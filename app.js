Date.prototype.toIsoString = function () {
    var tzo = -this.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-',
        pad = function (num) {
            var norm = Math.floor(Math.abs(num));
            return (norm < 10 ? '0' : '') + norm;
        };
    return this.getFullYear() +
        '-' + pad(this.getMonth() + 1) +
        '-' + pad(this.getDate()) +
        'T' + pad(this.getHours()) +
        ':' + pad(this.getMinutes()) +
        ':' + pad(this.getSeconds()) +
        dif + pad(tzo / 60) +
        ':' + pad(tzo % 60);
}



// Setup
var express = require('express');
var fileUpload = require('express-fileupload');
var app = express();
var session = require('express-session');
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

var MongoStore = require('connect-mongo')(session);

mongoose.connect("mongodb://localhost:27017/node-blog", { useNewUrlParser: true })
var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(fileUpload());
var CircularJSON = require('circular-json');


// Express Session
app.use(session({
    secret: 'oolalaa',
    saveUninitialized: true,
    resave: true,

    store: new MongoStore({
        mongooseConnection: mongoose.connection
    })
}));

var passport = require('passport')
    , LocalStrategy = require('passport-local').Strategy;

var User = require('./models/user')


// Passport init
app.use(passport.initialize());
app.use(passport.session());


app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    console.log("deserialize id " + id);
    User.findById(id, function (err, user) {
        console.log("user is" + user);
        done(err, user);
    });
});


var postSchema = new mongoose.Schema(
    {
        category: String,
        body: String,
        title: String,
        imageURL: String,
        ISOdate: String,
        viewcount: Number,
        likes: Number
    });

var Post = mongoose.model('Post', postSchema);


app.use(express.static('public'));

// Routes
app.get("/createpost", isAuthenticated, (req, res) => {
    //Post.find({}, (err, posts) => {
    res.render('create')
    //});
});

function isAuthenticated(req, res, next) {
    // do any checks you want to in here

    // console.log(req);
    // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
    // you can do this however you want with whatever variables you set up
    if (req.user)
        return next();

    // IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
    res.redirect('/login');
}

app.get("/", (req, res) => {
    Post.find({}, (err, posts) => {
        res.render('index', { posts: posts })
    });
});

app.get("/post/:postTitle", (req, res) => {

    var post = Post.findOne({ title: req.params.postTitle },
        (err, post) => {

            post.Year = post.ISOdate.substr(0, 4);
            switch (post.ISOdate.substr(5, 2)) {
                case 1:
                    post.Month = "იანვარი";
                    break;
                case 2:
                    post.Month = "თებერვალი";
                    break;
                case 3:
                    post.Month = "მარტი";
                    break;
                case 4:
                    post.Month = "აპრილი";
                    break;
                case 5:
                    post.Month = "მაისი";
                    break;
                case 6:
                    post.Month = "ივნისი";
                    break;
                case 7:
                    post.Month = "ივლისი";
                    break;
                case 8:
                    post.Month = "აგვისტო";
                    break;
                case 9:
                    post.Month = "სექტემბერი";
                    break;
                case 10:
                    post.Month = "ოქტომბერი";
                    break;
                case 11:
                    post.Month = "ნოემბერი";
                    break;
                default:
                    post.Month = "დეკემბერი";
            }

            post.Day = parseInt(post.ISOdate.substr(8, 2));

            res.render('single', { post: post });
        }


    );



});

app.get('/uploadfile', (req, res) => {

    res.render('upload');

});

app.post('/uploadfile', (req, res) => {

    var request = req;
    var response = res;

    var fs = require('fs');

    function uploadfunc(req, res) {
        if (!req.files)
            return res.status(400).send('No files were uploaded.');

        // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
        let sampleFile = req.files.fileToUpload;

        // Use the mv() method to place the file somewhere on your server
        sampleFile.mv('./public/images/news/' + sampleFile.name, function (err) {
            if (err)
                return res.status(500).send(err);

            res.send('File uploaded! : ' + sampleFile.name);
        });
    }


    uploadfunc(req, res)



});


app.post('/createpost', (req, res) => {

    console.log(CircularJSON.stringify(req));

    var postData = new Post(req.body);
    postData.ISOdate = (new Date()).toIsoString();

    postData.save().then(result => {
        console.log("POST SAVED");
        res.redirect('/');
    }).catch(err => {
        console.log("Unable to save data");
        res.status(400).send("Unable to save data");
    });
});


passport.use(new LocalStrategy(
    function (username, password, done) {
        User.getUserByUsername(username, function (err, user) {
            if (err) throw err;
            if (!user) {
                return done(null, false, { message: 'Unknown User' });
            }
            User.comparePassword(password, user.password, function (err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    return done(null, user);
                } else {
                    return done(null, false, { message: 'Invalid password' });
                }
            });
        });
    }
));

module.exports.createUser = function (newUser, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(newUser.password, salt, function (err, hash) {
            newUser.password = hash;
            newUser.save(callback);
        });
    });
}

module.exports.getUserByUsername = function (username, callback) {
    var query = { username: username };
    User.findOne(query, callback);
}

module.exports.getUserById = function (id, callback) {
    User.findById(id, callback);
}

module.exports.comparePassword = function (candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, function (err, isMatch) {
        if (err) throw err;
        callback(null, isMatch);
    });
}

app.get('/register', (req, res) => {
    res.render('register');
})


app.post('/register', function (req, res) {

    var password = req.body.password;
    var password2 = req.body.password2;

    if (password == password2) {
        var newUser = new User({
            username: req.body.username,
            password: req.body.password
        });

        User.createUser(newUser, function (err, user) {
            if (err) throw err;
            res.send(user).end()
        });
    } else {
        res.status(500).send("{errors: \"Passwords don't match\"}").end()
    }
});

//authentication - passport
app.get('/login', (req, res) => {

    res.render('login');

}

);

// Endpoint to login
app.post('/login',
    passport.authenticate('local'),
    function (req, res) {
        console.log("req.user is " + req.user);
        res.send(req.user);
    }
);

// Endpoint to get current user
app.get('/user', function (req, res) {
    res.send(req.user);
})


// Endpoint to logout
app.get('/logout', function (req, res) {
    req.session.destroy(function (err) {
        req.logout();
        res.send(null)
    });
});
// Listen
app.listen(3000, () => {
    console.log('Server listing on 3000');
})